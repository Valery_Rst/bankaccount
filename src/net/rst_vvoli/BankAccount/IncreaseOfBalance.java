package net.rstvvoli.bankAccount;

/**
 * Поток пополняет банковский счёт несколькими зачислениями;
 * Количество транзакций указано в константе (NUMBER_OF_TRANSACTIONS);
 * Количество денег, зачисляемые на счёт за одну транзакцию, указано в константе (AMOUNT_OF_MONEY_PER_TRANSACTION);
 */
public class IncreaseOfBalance extends Thread {
    private static final int NUMBER_OF_TRANSACTIONS = 10;
    private static final int AMOUNT_OF_MONEY_PER_TRANSACTION = 1_000;

    private final BankAccount account;
    IncreaseOfBalance(BankAccount account) {
        this.account = account;
    }

    @Override
    public void run() {
        synchronized (account) {
            for (int i = 0; i < NUMBER_OF_TRANSACTIONS; i++) {
                account.increaseOfCash(AMOUNT_OF_MONEY_PER_TRANSACTION);
                System.out.printf("Пополнение баланса на сумму: %d. Баланс: %s \n", AMOUNT_OF_MONEY_PER_TRANSACTION, account.getBalance());
            }
        }
    }
}