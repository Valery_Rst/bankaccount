package net.rstvvoli.bankAccount;

/**
 * Поток списывает деньги с банковского счёта одинаковыми частями;
 * Сумма списания за один раз указана в константе (GETTING_OF_MONEY_PER_TRANSACTION);
 */
public class GettingOfBalance extends Thread {
    private static final int GETTING_OF_MONEY_PER_TRANSACTION = 2_000;

    private final BankAccount account;

    GettingOfBalance(BankAccount account) {
        this.account = account;
    }

    @Override
    public void run() {
        synchronized (account) {
            while (account.getBalance() < GETTING_OF_MONEY_PER_TRANSACTION) {
                try {
                    System.out.printf("Ошибка. Для списания %d на балансе недостаточно средств. Необходимо пополнить баланс. \n", GETTING_OF_MONEY_PER_TRANSACTION);
                    account.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            while (account.getBalance() >= GETTING_OF_MONEY_PER_TRANSACTION) {
                account.notify();
                account.gettingOfMoney(GETTING_OF_MONEY_PER_TRANSACTION);
                System.out.printf("Снятие средств со счёта в размере %d . Баланс: %s \n", GETTING_OF_MONEY_PER_TRANSACTION, account.getBalance());
            }
        }
    }
}