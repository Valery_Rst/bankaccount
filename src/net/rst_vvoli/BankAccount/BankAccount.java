package net.rstvvoli.bankAccount;

/**
 * Представляет банковский счёт, который имеет баланс по умолчанию равный нулю,
 * в виде целочисленного количества денег;
 */
class BankAccount {
    private int balance;

    /**
     * Возвращает текущий баланс счёта;
     * @return текущий баланс счёта;
     */
    int getBalance() {
        return balance;
    }

    /**
     * Инициализирует объект как пустой счет, баланс которого равен 0;
     */
    BankAccount() {
        this.balance = 0;
    }

    /**
     * Увеличивает баланс на указанную сумму;
     * @param money сумма к пополнению;
     */
    void increaseOfCash(int money){
        balance += money;
    }

    /**
     * Уменьшает текущий баланс на указанную сумму;
     * @param money сумма к списанию;
     */
    void gettingOfMoney(int money) {
        balance -= money;
    }
}