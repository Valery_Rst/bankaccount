package net.rstvvoli.bankAccount;

public class MainDemo  {
    public static void main (String[] args) {
        BankAccount account = new BankAccount();

        IncreaseOfBalance increaseOfBalance = new IncreaseOfBalance(account);
        GettingOfBalance gettingOfBalance = new GettingOfBalance(account);

        increaseOfBalance.start();
        gettingOfBalance.start();
    }
}
